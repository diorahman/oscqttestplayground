#include <QtGui/QGuiApplication>
#include "qtquick2applicationviewer.h"

#include <QQuickItem>
#include "oscsender.h"

int main(int argc, char *argv[])
{
    QGuiApplication app(argc, argv);

    qmlRegisterType<OSCSender>("Test", 1, 0, "Sender");

    QtQuick2ApplicationViewer viewer;
    viewer.setMainQmlFile(QStringLiteral("qml/PlaygroundSender/main.qml"));
    viewer.showExpanded();

    return app.exec();
}
