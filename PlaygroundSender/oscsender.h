#ifndef OSCSENDER_H
#define OSCSENDER_H

#include <QObject>
#include "qoscclient.h"

class OSCSender : public QObject
{
    Q_OBJECT
public:
    explicit OSCSender(QObject *parent = 0);

    Q_INVOKABLE void send(const QString & path, const int & val);
    
signals:
    
public slots:

private:
    QOscClient * m_sender;
    
};

#endif // OSCSENDER_H
