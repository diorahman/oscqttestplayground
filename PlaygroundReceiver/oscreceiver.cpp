#include "oscreceiver.h"
#include <QDebug>

OSCReceiver::OSCReceiver(QObject *parent) :
    QObject(parent)
{
    m_receiver = new QOscServer(3333, this);
    m_userActive = new PathObject( "/1/userActive", QVariant::Int, m_receiver );
    m_userInactive = new PathObject( "/1/userInactive", QVariant::Int, m_receiver );
    m_rightHandSwipeUp = new PathObject( "/1/rightHand/swipeUp", QVariant::Int, m_receiver );
    m_rightHandSwipeDown = new PathObject( "/1/rightHand/swipeDown", QVariant::Int, m_receiver );
    m_rightHandSwipeLeft = new PathObject( "/1/rightHand/swipeLeft", QVariant::Int, m_receiver );
    m_rightHandSwipeRight = new PathObject( "/1/rightHand/swipeRight", QVariant::Int, m_receiver );

    connect( m_userActive, SIGNAL(data(int)), this, SLOT( receivedData(int)));
    connect( m_userInactive, SIGNAL(data(int)), this, SLOT( receivedData(int)));
    connect( m_rightHandSwipeUp, SIGNAL(data(int)), this, SLOT( receivedData(int)));
    connect( m_rightHandSwipeDown, SIGNAL(data(int)), this, SLOT( receivedData(int)));
    connect( m_rightHandSwipeLeft, SIGNAL(data(int)), this, SLOT( receivedData(int)));
    connect( m_rightHandSwipeRight, SIGNAL(data(int)), this, SLOT( receivedData(int)));
}

void OSCReceiver::receivedData(const int &val)
{
    PathObject * obj = qobject_cast<PathObject * >(sender());
    qDebug() << obj->path() << " - " << val;
    emit data(obj->path(), val);
}
