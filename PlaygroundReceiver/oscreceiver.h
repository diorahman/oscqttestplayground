#ifndef OSCRECEIVER_H
#define OSCRECEIVER_H

#include <QObject>
#include "qoscserver.h"

class OSCReceiver : public QObject
{
    Q_OBJECT
public:
    explicit OSCReceiver(QObject *parent = 0);
    
signals:
    void data(const QString & path, const int & val);
    
public slots:

private slots:
    void receivedData(const int & val);

private:
    QOscServer * m_receiver;
    PathObject * m_userActive;
    PathObject * m_userInactive;
    PathObject * m_rightHandSwipeUp;
    PathObject * m_rightHandSwipeDown;
    PathObject * m_rightHandSwipeLeft;
    PathObject * m_rightHandSwipeRight;
    
};

#endif // OSCRECEIVER_H
